﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CanvasButtons : MonoBehaviour
{
    public Sprite musicOn, musicOff;

    public void Start()
    {
        if (PlayerPrefs.GetString("music") == "No" && gameObject.name == "Music")
        {
            GetComponent<Image>().sprite = musicOff;
        }
    }

    public void Restart()
    {
        ClickMusic();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Instagram()
    {
        ClickMusic();
        Application.OpenURL("https://www.instagram.com/drz.vladdz/?hl=ru");
    }

    /* Button on/off volume */
    public void Music()
    {
        if(PlayerPrefs.GetString("music") == "No") {
            PlayerPrefs.SetString("music", "Yes");
            GetComponent<AudioSource>().Play();
            GetComponent<Image>().sprite = musicOn;
        } else
        {
            PlayerPrefs.SetString("music", "No");
            GetComponent<Image>().sprite = musicOff;
        }
    }


    /* Shop */
    /* Button Shop */
    public void Shop()
    {
        ClickMusic();
        SceneManager.LoadScene("Shop");
    }

    public void CloseShop()
    {
        ClickMusic();
        SceneManager.LoadScene("Main");
    }

    private void ClickMusic()
    {
        if (PlayerPrefs.GetString("music") != "No")
        {
            GetComponent<AudioSource>().Play();
        }
    }
}