﻿using UnityEngine;

public class ExplodeCubes : MonoBehaviour
{
    public GameObject restart, explosion;
    private bool _collisionSet;

    private void OnCollisionEnter(Collision Cubes)
    {
        if(Cubes.gameObject.tag == "Cube" && !_collisionSet)
        {
            // Effect Shake camera
            Camera.main.gameObject.AddComponent<CameraShake>();

            addCubesRigitBody(Cubes);
            effectExplode(Cubes);
            musicExplode();

            Destroy(Cubes.gameObject);
            _collisionSet = true;

            // Show button Restart
            restart.SetActive(true);
        }
    }
    private void addCubesRigitBody(Collision Cubes)
    {
        for (int i = Cubes.transform.childCount - 1; i >= 0; i--)
        {
            Transform child = Cubes.transform.GetChild(i);
            child.gameObject.AddComponent<Rigidbody>();
            child.gameObject.GetComponent<Rigidbody>().AddExplosionForce(70f, Vector3.up, 5f);
            child.SetParent(null);
        }
    }

    private void effectExplode(Collision Cubes)
    {
        GameObject newVfx = Instantiate(explosion, new Vector3(Cubes.contacts[0].point.x, Cubes.contacts[0].point.y, Cubes.contacts[0].point.z), Quaternion.identity) as GameObject;
        Destroy(newVfx, 2.5f);
    }

    private void musicExplode()
    {
        if (PlayerPrefs.GetString("music") != "No")
            GetComponent<AudioSource>().Play();
    }

}
