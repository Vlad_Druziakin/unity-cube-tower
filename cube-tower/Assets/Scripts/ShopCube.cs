﻿using UnityEngine;

public class ShopCube : MonoBehaviour
{
    public int closedToTheBestResult; // Set in the Unity what should be the best result
    public Material closeMaterial;// Set in the Unity close Material

    void Start()
    {
        // If the best result is less than what is required for opening, then we use the black material
        if (PlayerPrefs.GetInt("score") < closedToTheBestResult)
            GetComponent<MeshRenderer>().material = closeMaterial;
    }
}

